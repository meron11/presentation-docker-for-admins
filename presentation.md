---
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
marp: true
---

![bg left:40% 80%](img/docker-logo.png)

# **Docker**

Dla administratorów

Marcin Golonka
https://halfbitstudio.com

---

# Plan Prezentacji
* Wprowadzenie
* Docker a wirtualizacja
* Instalacja i podstawy
  <!-- * Linux
  * Windows
  * Podstawowe komendy -->
* Docker Compose
* Studium przypadku
* Podsumowanie

---

<!-- _class: lead -->
# Wprowadzenie

---
# Konteneryzacja

* Umieszczanie aplikacji w separowanych środowiskach
* Większa wydajność względem wirtualizacji
  * Brak konieczności emulowania całego PC
* Wspierana w jądrze systemu
  * **Wyjątek Windows**
* Abstrakcyjność względem systemu
  *  Izolowanie systemu pliku
  *  Możliwośc narzucania limitów
---
# Historia Konteneryzacji
* 2004 Solaris Containers
* 2008 Linux Containers
* 2013 **dotCloud Docker**

### Alternatywne implementacje
* Chroot
* Sysjail
* ...

---
# Docker
* Open Source
* Multiplatformowy
* Emulujący warstwę sieciową
* Możliwość pracy wieloserwerowej
* Wysokie bezpieczeństwo
* ...

---
<!-- _class: lead -->
# Docker a wirtualizacja

---
![bg contain](img/docker_vm.png)

---
![bg contain](img/docker_containers.png)

---

# Docker - Podstawowe pojęcia
* Obraz - Składająca się z warstw, reprezentacja aplikacji
* Kontener - Instancja obrazu
* Repozytorium - baza danych obrazów

---
<!-- _class: lead -->
# Instalacja

---

## Debian
```bash
#Dodajemy klucz którym jest podpisane reposytorium
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -  
# Dodajemy repozytorium
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
# Aktualizacja rejestru pakietów i instalacja dockera
apt-update && apt-get install docker-ce
```
---
## Fedora

```bash
dnf -y install dnf-plugins-core
dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
dnf install docker-ce docker-ce-cli containerd.io
```

---
# Windows 10

https://download.docker.com/win/stable/Docker%20Desktop%20Installer.exe

---

<!-- _class: lead -->
# Podstawy

---
# Podstawowe komendy
### Uruchomienie kontenera
```bash
docker run <nazwa obrazu>
```
### Zatrzymanie kontenera
```bash
docker stop <nazwa kontenera>
```

### Lista uruchomionych kontenerów
```bash
docker ps
```
---

# Hello World

```bash
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
1b930d010525: Pull complete
Digest: sha256:fc6a51919cfeb2e6763f62b6d9e8815acbf7cd2e476ea353743570610737b752
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
[...]
```

---
# Nginx
```bash
docker run  -v $(pwd):/usr/share/nginx/html -p 9090:80 nginx 
```

### Woluminy
`docker run --name demo1`**`-v $(pwd):/usr/share/nginx/html`**`-p 9090:80 nginx`

### Przekierowanie portów
`docker run  --name demo1 -v $(pwd):/usr/share/nginx/html`**`-p 9090:80`** `nginx`


---
# Przekierowywanie portów
Mechanizm pozwalający wykonać routing ruchu sieciowego z kart sieciowych hosta na kontener
Jest podstawą izolacji kontenerów od świata zewnętrznego

---
![bg contain](img/diagram_porty.png)


---
<!-- _class: lead -->
# Docker Compose

---
# Docker Compose
* Oparty o YAML
* Pozwala w sposób wygodny opisać wielo-kontenerową infrastrukturę
* OpenSource
* Uruchamianie wielu kontenerów jednym poleceniem
* Struktura pliku ściśle określona
---
# Instalacja
## Linux
```bash
 curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
 chmod a+x /usr/local/bin/docker-compose
```
## Windows
Zainstalowany wraz z Dockerem :tada:

---

<!-- _class: lead -->
# Studium przypadku

---
# Wymagane usługi
  * Wordpress
  * MariaDB
  * Rocket.Chat

---

## Podstawowy plik docker-compose.yml
```yaml
version: '3'
services:
```

---

<!-- _class: lead -->
# Wordpress

---


```yaml
[...]
services:
  wordpress:
      image: wordpress
      links:
      - mariadb:mysql
      environment:
      - WORDPRESS_DB_PASSWORD=haslo
      ports:
      - "9090:80"
  mariadb:
      image: mariadb
      environment:
        - MYSQL_ROOT_PASSWORD=haslo
        - MYSQL_DATABASE=wordpress
      volumes:
        - maria_db:/var/lib/mysql
volumes:
  maria_db:
```

---

<!-- _class: lead -->
# Rocket.Chat

---


```yaml
[...]
services:
  rocketchat:
    image: rocket.chat:latest
    restart: unless-stopped
    volumes:
      - uploads:/app/uploads
    environment:
      - PORT=3000
      - ROOT_URL=http://localhost
      - MONGO_URL=mongodb://mongo:27017/rocketchat
      - MONGO_OPLOG_URL=mongodb://mongo:27017/local
      - Accounts_UseDNSDomainCheck=False
    depends_on:
      - mongo
    ports:
      - 8818:3000
  mongo:
    image: mongo:4.0
    restart: unless-stopped
    volumes:
     - data:/data/db
     - dump:/dump
    command: mongod --smallfiles --oplogSize 128 --replSet rs0 --storageEngine=mmapv1
  mongo-init-replica:
    image: mongo
    command: 'bash -c "for i in `seq 1 30`;
     do mongo mongo/rocketchat --eval \"rs.initiate({ _id: ''rs0'', members: [ { _id: 0, host: ''localhost:27017'' } ]})\"
      && s=$$? && break || s=$$?; echo \"Tried $$i times. Waiting 5 secs...\"; sleep 5; done; (exit $$s)"'
    depends_on:
      - mongo
[...]
volumes:
  data:
  dump:
  uploads:
```

---

# Uruchomienie

```bash
docker-compose up -d 
```
Przełącznik `-d` odłącza logi usług od konsoli. Zaczynają one pracować w tle.


---

<!-- _class: lead -->
# Podsumowanie

---

<!-- _class: lead -->
# Pytania?


---

<!-- _class: lead -->
# Dziękuję za uwagę

---
# Prezentacja

https://bitbucket.org/meron11/presentation-docker-for-admins